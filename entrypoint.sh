#!/bin/bash

# Check update of Symfony CLI
yes | symfony self-update

# If certs not installed, generate auto-signed
if [ ! "$(ls -A $HOME/.symfony/certs 2> /dev/null)" ]; then
	symfony server:ca:install
fi

if [ -f "composer.json" ]; then
	# Install dependencies
	symfony composer install
fi

# Start services
if [ "$(id -u -n)" == "root" ]; then
	service cron start
fi

# Action only in non production environment
if [ ! $PROD ]; then
	if [ $YARN_ENCORE_WATCH ]; then
		# Enable Yarn Encore watching
		symfony run -d yarn encore dev --watch
	fi
fi

# Start Web Server
symfony server:start --port=80 --allow-http=true
