FROM ubuntu:22.04
MAINTAINER Carlo Cassano <info@carloweb.it>
EXPOSE 80 443
VOLUME ["/srv/symfony"]

ENV DEBIAN_FRONTEND noninteractive

ENV PROD false
ENV YARN_ENCORE_WATCH false

# Update packages
RUN apt-get update

# Install PHP PPA for PHP
RUN apt-get install -my software-properties-common && \
  add-apt-repository -y ppa:ondrej/php && \
  apt-get update

# Install packages
RUN apt-get install -my curl \
    wget \
    php8.2-curl \
    php8.2-gd \
    php8.2-xsl \
    php8.2-mysqlnd \
    php8.2-cli \
    php8.2-intl \
    php8.2-bz2 \
    php8.2-zip \
    php8.2-mbstring \
    php8.2-dev \
    php-pear \
    php8.2-imagick \
    php8.2-soap \
    php8.2-mysql \
    php8.2-sqlite3 \
    libmcrypt-dev \
    libcap2-bin \
    git \
    zip && \
  rm -rf /var/lib/apt/lists/*

# Install mcrypt with PECL
RUN pecl channel-update pecl.php.net && \
  yes '' | pecl install -f mcrypt-1.0.5 && \
  pecl clear-cache && \
  rm -rf /tmp/pear

# Download Composer
RUN cd /tmp && \
  php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');" && \
  php composer-setup.php && \
  php -r "unlink('composer-setup.php');" && \
  mv composer.phar /usr/bin/composer

# Download Symfony CLI
RUN wget https://get.symfony.com/cli/installer -O - | bash && mv /root/.symfony5/bin/symfony /usr/local/bin/symfony

# Set permissions for non root users
RUN chmod 777 /srv/symfony && \
  setcap CAP_NET_BIND_SERVICE=+eip /usr/local/bin/symfony

# Install Node.js
RUN curl -sL https://deb.nodesource.com/setup_18.x | bash - && \
  apt-get install -my nodejs && \
  rm -rf /var/lib/apt/lists/*

# Install Yarn
RUN curl -sL https://dl.yarnpkg.com/debian/pubkey.gpg | gpg --dearmor | tee /usr/share/keyrings/yarnkey.gpg >/dev/null && \
  echo "deb [signed-by=/usr/share/keyrings/yarnkey.gpg] https://dl.yarnpkg.com/debian stable main" | tee /etc/apt/sources.list.d/yarn.list && \
  apt-get update && \
  apt-get install -my yarn && \
  rm -rf /var/lib/apt/lists/*

COPY entrypoint.sh /srv/entrypoint.sh

WORKDIR /srv/symfony

ENTRYPOINT /srv/entrypoint.sh
